+++
title = "Datasets"
weight = 50
chapter = false
+++

List of datasets currently stored on the compute volume

### How to provide a dataset?

request a directory for the compute volume, Put the dataset there, set read only, document here

### Listing

{{% children style="h2" description="true" showhidden="true" %}}