---
title: "Cluster Nodes"
weight: 50
date: 2021-10-28T13:41:58+02:00
---

## Cluster Nodes


|Hostname    |CPU-Cores|RAM   |GPU            |
|------------|---------|------|---------------|
|papaschlumpf|20       |256 GB|2 x Tesla P-100|
|schlumpfine |20       |256 GB|2 x Tesla P-100|
|schlaubi    |20       |128 GB|2 x GTX 1080 TI|
|hefty       |20       |128 GB|2 x GTX 1080 TI|
|clumsy      |20       |128 GB|2 x GTX 1080 TI|
|handy       |20       |128 GB|2 x GTX 1080 TI|