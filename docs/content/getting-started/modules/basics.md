+++
title = "Basics"
date = 2021-10-28T13:07:12+02:00
weight = 5
+++

## Environment Modules Basics

Environment modules supports you in managing multiple tool flow versions like compilers or libraries. Therefore it manages user environment variables and keeps control of e.g. `PATH`, `LD_LIBRARY_PATH`, `INCLUDE` and other environment variables. Environment Modules benefits of the runtime extension and reduction of modules controlled by the user.

## currently available modules

* cuda/6.5
* cuda/7.5
* cuda/8.0
* cuda/9.0
* license
* matlab/R2017b
* matlab/R2018b

## Examples

show available modules

```
module avail
```

show loaded modules

```
module list
```

load a module

```
module load cuda/9.0
```

unload a module

```
module remove cuda/9.0
```

see `/media/compute/vol/tutorial`