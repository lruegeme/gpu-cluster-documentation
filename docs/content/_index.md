---
title: "Home Page"
date: 2021-10-28T12:46:07+02:00
---

# CIT-EC GPU Cluster

The GPU compute cluster currently offers several hybrid nodes with balanced cpu/ram and gpu ratio. Access maybe granted upon request by additional user permissions. The cluster utilizes the workload manager Slurm. You may start your jobs via the login node *login.gpu.cit-ec.net*. For Compute tasks it is mandatory to use the slurm scheduler.

Although the cluster nodes running the TechFak netboot installation, the locations `/homes` and `/vol` are not available. Therefore an exclusive homes and vol is located at `/media/compute/` On initial login your compute home directory will be provisioned under `/media/compute/homes/user`. It is separated from your regular TechFak home location `/homes/user`. The compute home is accessible via [files.techfak.de](https://www.techfak.net/dienste/remote/files) and a regular TechFak netboot system like *compute.techfak.de*.

## Support

On the [Universities Matrix Service](https://matrix.uni-bielefeld.de/) join the Channel `#citec-gpu:uni-bielefeld.de` or send a support request to [GPU Support](mailto:gpu-support@techfak.de)



